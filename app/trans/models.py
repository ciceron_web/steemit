from app import app, db, common, mail
from sqlalchemy import MetaData, Table
import traceback
import re
from markdown import markdown
from bs4 import BeautifulSoup as BS
from datetime import datetime
import requests
import json
from pprint import pprint

from steem import Steem
from steem.post import Post
s = Steem(keys=app.config['STEEM_POSTING_KEY'])


def get_post(link):
    permlink = '@{}'.format(link.split('@', maxsplit=1)[-1])
    post = Post(post=permlink).export()
    return permlink, post


def estimate_and_save(link):
    """
    1. 입력된 URL을 이용하여 포스트 데이터와 작성자 데이터를 얻는다
    2. 데이터베이스에 저장한다
    3. 포스트의 원문 언어를 찾아낸다 (Google API 사용)
    4. 예상 가격을 측정한다

    :return: 사용자이름, 원문 언어, 포스트 내용, 에상 가격
    """
    permlink, post = get_post(link)

    #: 3. 포스트의 원문 언어 얻어오기
    lang = detect_language(post['body'])

    #: 4. 예상 가격 측정하기
    price, str_cnt = set_price(post['title'], post['body'])

    #: 2-1. 포스트 데이터 저장하기
    is_done = insert_post_and_user(post, link, lang, price)
    if is_done is False:
        return False, None

    result = {
        'username': post['author'],
        'content': post['body'],
        'ol': lang,
        'price': price,
        'str_cnt': str_cnt
    }
    return True, result


def detect_language(content):
    """
    참고: https://cloud.google.com/translate/docs/reference/detect

    {'data': {'detections': [[{'confidence': 1,
                           'isReliable': False,
                           'language': 'ko'}]]}}
    :param content: 포스트 내용
    :return: content의 원문 언어
    """
    res = requests.post('https://translation.googleapis.com/language/translate/v2/detect',
                        data={'q': content, 'key': app.config['GOOGLE_API_KEY']})
    data = res.json()
    language = data['data']['detections'][0][0]['language']
    return language


def set_price(title, content):
    """
    1. http~와 같은 링크와 특수문자를 삭제한다
    2. 스팀용 씨세론 번역비 계산법에 의하여 가격을 측정한다
    :param content: 포스트 내용
    :return: 금액
    """
    #: 마크다운 문서에서 글자만 뽑아내기
    html = markdown(content)
    soup = BS(html, 'lxml')
    sentences = soup.find_all(text=True)
    sentences.insert(0, title)
    text = ''.join(sentences)

    #: link & 특수문자 삭제하기
    t1 = re.sub(r'http\S+', '', text, flags=re.MULTILINE)
    t2 = re.sub('\W', '', t1)

    #: 금액 계산하기
    str_cnt = len(t2)
    price = round(str_cnt * 0.0001, 3)
    return price, str_cnt


def insert_post_and_user(post, link, lang, price):
    conn = db.engine.connect()
    trans = conn.begin()
    meta = MetaData(bind=db.engine)
    t1 = Table('post', meta, autoload=True)
    t2 = Table('users', meta, autoload=True)

    #: 작성자 데이터 가져오기
    account = s.get_account(post['author'])

    try:
        conn.execute(t1.insert(), post_data=json.dumps(post, default=lambda x: str(x)), link=link,
                     origin_lang=lang, price=price, total_vote_weight=post['total_vote_weight'],
                     vote_rshares=post['vote_rshares'], title=post['title'], body=post['body'], tags=post['tags'],
                     active_votes=post['active_votes'], pending_payout_value=post['pending_payout_value'])

        conn.execute(t2.insert(), user_data=account, username=account['name'], post_count=account['post_count'],
                     voting_power=account['voting_power'], reputation=account['reputation'])

        trans.commit()
        return True
    except:
        traceback.print_exc()
        trans.rollback()
        return False


def request_trans(link, origin_lang, trans_lang, posting_key, email, price, str_cnt, memo):
    """
    1. 데이터베이스에 번역 의뢰 요청정보를 저장한다
    2. 댓글로 번역 의뢰 내용을 작성한다
    3. 씨세론 관리자에게 번역 의뢰 이메일을 보낸다
    :return: OK
    """
    formatto = "%Y-%m-%d %H:%M"
    permlink, post = get_post(link)
    trans_data = {
        'price': price,
        'str_cnt': str_cnt,
        'status': '입금대기',
        'origin_lang': origin_lang,
        'trans_lang': trans_lang,
        'client_name': post['author'],
        'client_email': email,
        'posting_key': posting_key,
        'link': link,
        'transaction_id': common.create_token(link, 20),
        'client_memo': memo,
        'request_time': datetime.strftime(datetime.now(), formatto)
    }

    # 1. 데이터베이스에 번역 의뢰 요청정보를 저장한다
    is_done = insert_trans_request(trans_data)
    if is_done is False:
        return False

    # 2. 댓글로 번역 의뢰 내용을 작성한다
    is_done = write_comment(permlink, trans_data)
    if is_done is False:
        return False

    # 3. 씨세론 관리자와 고객에게 번역 의뢰 접수 이메일을 보낸다
    is_done = send_request_sent_confirm_mail(email, post['author'], origin_lang, trans_lang, str_cnt)
    if is_done is False:
        return False
    return True


def insert_trans_request(data):
    conn = db.engine.connect()
    trans = conn.begin()
    meta = MetaData(bind=db.engine)
    t1 = Table('request', meta, autoload=True)

    #: 데이터베이스에 저장하기
    try:
        conn.execute(t1.insert(), **data)

        trans.commit()
        return True
    except:
        traceback.print_exc()
        trans.rollback()
        return False


def write_comment(permlink, data):
    title = '포스팅 번역 견적입니다'
    comment = """## `{client_name}`님이 의뢰하신 번역 견적서입니다
\n
| 의뢰시간 | 언어 | 금액 | 거래번호 |
| :--------: | :-----: | :---: | :-----------: |
| {request_time} | {origin_lang} > {trans_lang} | $ {price} | {transaction_id} |\n
> - 최초 공급가액에는 번역 및 초벌감수까지 포함되어 있습니다.
> - 번역물 제공 후 과하지 않은 선에서 추가 문의 및 검수 요청은 가능합니다.""".format(**data)

    try:
        s.commit.post(title=title,
                      body=comment,
                      author='sunho.lee',
                      permlink=None,  # Manually set the permlink (defaults to None). If left empty, it will be derived from title automatically
                      reply_identifier=permlink,  # Identifier of the parent post/comment (only if this post is a reply/comment) (eg. @author/permlink)
                      json_metadata=None,
                      comment_options=None,
                      # comment_options = {
                      #     'max_accepted_payout': '1000000.000 SBD',
                      #     'percent_steem_dollars': 10000,
                      #     'allow_votes': True,
                      #     'allow_curation_rewards': True,
                      #     'extensions': [
                      #         [0,
                      #          {
                      #              'beneficiaries': [
                      #                  {'account': 'account1', 'weight': 5000},
                      #                  {'account': 'account2', 'weight': 5000},
                      #              ]
                      #          }
                      #         ]
                      #     ]
                      # },
                      community=None,
                      tags=None,  # (str, list)
                      # (Optional) A list of tags (5 max) to go with the post. This will also override the tags specified in json_metadata.
                      # The first tag will be used as a ‘category’. If provided as a string, it should be space separated.
                      beneficiaries=None,
                      self_vote=False)
        return True
    except:
        traceback.print_exc()
        return False


def send_request_sent_confirm_mail(email, client_name, ol, tl, str_cnt):
    admin = {
        'mail_from': email,
        'mail_to': ['sunny@ciceron.me'],  # 스팀잇 담당자
        'subject': '스팀잇에서 의뢰가 들어왔습니다.',
        'which_mail': 'request_sent_confirm'
    }
    client = {
        'mail_from': 'no-reply@ciceron.me',
        'mail_to': [email],
        'subject': '의뢰 접수 안내/Confirmation of the request',
        'which_mail': 'request_sent_confirm'
    }
    admin_data = {
        'main_text': '의뢰가 들어왔습니다.',
        'sub_text': '확인 후 가격을 지정해주세요.',
        'mail_text': '의뢰가 완료되었습니다. 가격을 지정해주세요.'
    }
    client_data = {
        'main_text': '의뢰가 성공적으로 접수되었습니다.',
        'sub_text': '입금이 완료되면 번역이 진행됩니다. 감사합니다.',
        'mail_text': '의뢰 접수가 완료되었습니다. 결제를 완료해주세요. 감사합니다.'
    }
    data = {
        'email': email,
        'name': client_name,
        'message': '스팀잇 만들면서 메일 모듈 정리중입니다.',
        'original_language': ol,
        'target_language': tl,
        'deadline_date': None,
        'text_counter': str_cnt,
        'coupon': None,
        'filename': None,
        'filebin': None
    }

    #: 씨세론 담당자에게 이메일 보내기
    is_done = mail.send_mail(admin, dict(admin_data, **data))
    if is_done is False:
        return False

    #: 고객에게 이메일 보내기
    is_done = mail.send_mail(client, dict(client_data, **data))
    if is_done is False:
        return False
    return True
