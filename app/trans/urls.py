from flask import Blueprint
import app.trans.controllers as ctrl

trans = Blueprint('steem', __name__)

trans.add_url_rule('/poking', view_func=ctrl.estimate_trans, methods=['POST'])
trans.add_url_rule('/request', view_func=ctrl.request_trans, methods=['POST'])
