from flask import request, make_response, jsonify
import app.trans.models as model


def estimate_trans():
    """
    입력된 URL을 이용하여 포스트 데이터와 작성자 데이터를 얻는다
    데이터베이스에 저장한다
    번역 원문 언어를 찾아낸다 (Google API 사용)
    예상 가격을 측정한다

    :return: 사용자이름, 원문 언어, 포스트 내용, 에상 가격
    """
    link = request.form.get('link', None)

    is_done, data = model.estimate_and_save(link)
    if is_done is True:
        return make_response(jsonify(**data), 200)
    else:
        return make_response(jsonify(result='Something Wrong'), 461)


def request_trans():
    """
    데이터베이스에 번역 의뢰 요청정보를 저장한다
    댓글로 번역 의뢰 내용을 작성한다
    씨세론 관리자에게 번역 의뢰 이메일을 보낸다
    :return: OK
    """
    link = request.form.get('link', None)
    origin_lang = request.form.get('ol', None)
    trans_lang = request.form.get('tl', None)
    posting_key = request.form.get('pk', None)
    email = request.form.get('email', None)
    price = request.form.get('price', None)
    str_cnt = request.form.get('cnt', None)
    content = request.form.get('content', None)
    memo = request.form.get('memo', None)

    if None in [link, origin_lang, trans_lang, email, price, str_cnt]:
        return make_response(jsonify(result='Something Not Entered'), 460)

    is_done = model.request_trans(link, origin_lang, trans_lang, posting_key, email, price, str_cnt, memo)
    if is_done is True:
        return make_response(jsonify(result='OK'), 200)
    else:
        return make_response(jsonify(result='Something Wrong'), 461)
