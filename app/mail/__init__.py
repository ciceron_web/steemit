import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
import traceback
from pprint import pprint

LOGIN_EMAIL = 'no-reply@ciceron.me'
LOGIN_PASSWORD = 'ciceron3388!'


def send_mail(info, content):
    """
    기본이 되는 함수입니다.
    
    :param info: 
        mail_info = {
            'mail_from': email,
            'mail_to': ['sunny@ciceron.me'],
            'subject': '이메일 제목',
            'which_mail': '보낼 html 파일 이름'
        }
        :mail_from: 보내는 사람
        :mail_to: 받는 사람. 여러명이 받는 경우를 위해 배열로 받습니다. ex) ['chingoo@ciceron.me', 'betty@ciceron.me']
        :subject: 이메일 제목
        :which_mail: 어떤 메일을 보내는가
        
    :param content: which_mail에 따라서 내용이 달라집니다.
    """
    try:
        msg = MIMEMultipart()

        msg['To'] = ', '.join(info['mail_to'])
        msg['From'] = info['mail_from']
        msg['Subject'] = info['subject']

        #: 메일 내용 채우기
        with open('app/mail/templates/{}.html'.format(info['which_mail']), 'r') as f:
            html = f.read().format(**content)
        content = MIMEText(html, 'html', _charset='utf-8')
        msg.attach(content)

        #: 첨부파일 붙이기
        filename = content['filename']
        filebin = content['filebin']
        if not None in [filename, filebin]:
            attachment = MIMEApplication(filebin, Name=filename)
            attachment['Content-Disposition'] = 'attachment; filename={}'.format(filename)
            msg.attach(attachment)

        #: 메일 전송
        a = smtplib.SMTP('smtp.gmail.com:587')
        a.ehlo()
        a.starttls()
        a.login(LOGIN_EMAIL, LOGIN_PASSWORD)
        a.sendmail(info['mail_from'], info['mail_to'], msg.as_string())
        a.quit()

        return True
    except:
        traceback.print_exc()
        return False


def _send_mail(mail_to, subject, message, mail_from='no-reply@ciceron.me', name='CICERON', password='ciceron3388!'):
    import smtplib
    import base64
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText

    content = MIMEText(message, 'html', _charset='utf-8')
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = '{} team <{}>'.format(name, mail_from)
    msg['To'] = str(mail_to)
    msg.attach(content)

    print(msg)
    a = smtplib.SMTP('smtp.gmail.com:587')
    a.ehlo()
    a.starttls()
    a.login(mail_from, password)
    a.sendmail(mail_from, str(mail_to), msg.as_string())
    a.quit()
